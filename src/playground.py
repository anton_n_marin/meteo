from data_processing.useful_functions import *

from data_processing.data_extraction import LMeteo3DBExtractor
from data_processing.data_filters import CheckRangeLMeteo3Filter, GroupLMeteo3ByStationDataFilter, \
    SplitGroupedDataFilter, SplittedArrayToSamplesFilter
import datetime
import pickle

data_extractor = LMeteo3DBExtractor()
stations = [113, 114, 115, 116, 117, 119, 302, 303, 304, 305, 306, 307, 308, 309, 393, 442,
           502, 503, 504, 505, 506, 507, 508, 511, 512, 513, 514, 515, 516, 591, 592, 593,
           594, 596, 597, 598, 599, 612, 613, 614, 615, 616, 617, 618, 619, 620, 621, 622,
           623, 624, 625, 626, 627, 628, 629, 836, 837, 838, 839, 862, 863, 874, 875, 888,
           1820, 1832, 1833, 1834, 1835, 1836, 1838, 1896, 1899]
#stations = [113]
datetime_start = datetime.datetime(year=2015, month=6, day=1, hour=0, minute=0, second=0)
datetime_end = datetime.datetime(year=2016, month=1, day=1, hour=0, minute=0, second=0)
sensor_types = [1, 2, 3, 4, 16]
lmeteo_raw = data_extractor.extract(db_connection='mysql://root:toor@127.0.0.1:3306/lmeteo3', stations=stations,
                        datetime_start=datetime_start, datetime_end=datetime_end,
                        sensor_types=sensor_types, limit=1000000)

check_range_filter=CheckRangeLMeteo3Filter()
lmeteo_range_checked = check_range_filter.apply(lmeteo_raw)

#############
meteo_group_filter = GroupLMeteo3ByStationDataFilter()
meteo_grouped = meteo_group_filter.apply(lmeteo_range_checked)
#############

split_grouped_filter = SplitGroupedDataFilter()
meteo_splitted = split_grouped_filter.apply(meteo_grouped)

#############
samples_filter = SplittedArrayToSamplesFilter()
samples = samples_filter.apply(meteo_splitted)
print(str(samples))

with open('pickle/50000_samples.pickle', 'wb') as f:
    pickle.dump(samples, f)
