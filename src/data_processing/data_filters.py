from abc import ABCMeta

import pandas
from scipy.interpolate import interp1d

from data_processing.constants import T_MIN, T_MAX, HU_MIN, HU_MAX, PA_MIN, PA_MAX, dt_max, dT_min
from data_processing.data_types import MeteoGroupedByStationData, MeteoSplittedArray, MeteoSample, \
    MeteoSampleArray, MeteoDataArray, MeteoDataFrame
from data_processing.useful_functions import *


class DataTransformer:
    '''!
    Defines abstract data transformation class. Checks input type of the data and creates data of given type
    Contains function, which transforms tha data

    '''
    __metaclass__ = ABCMeta

    def __init__(self, input_type, output_type, func=None):
        '''!
        @param input_type - inheritor of DataAbstract, input
        '''
        self._input_type = input_type
        self._output_type = output_type
        self._func = func

    def apply(self, data):
        '''!
        Applies function to the input data and creates output data
        @param data - input data
        '''
        pass


class Filter(DataTransformer):
    '''!
    Unary data transormation, which makes no assumptions on input data structure (anyway input data must be
    of a type, derivative of DataAbstract
    '''
    def check_type(self, data):
        '''!
        Checks, if input data type matches self._input_type. Otherwise raise an exception
        '''
        try:
            assert type(data) == self._input_type
        except:
            raise AssertionError('Can\'t apply transformation. Got type: ' + \
                                 str(type(data)) + ', expected: ' + str(self._input_type))

    def apply(self, data):
        '''!
        Applies self._func to the given data and creates data of self._output_type by invoking its constructor.
        '''
        # checking data type
        self.check_type(data)
        return self._output_type(self._func(data))


class FilterData(Filter):
    '''!
    Filter, where self._func is applied to 'data' member of the input data
    '''
    def apply(self, data):
        '''!
        Applies self._func to the 'data' member of the input data
        and creates data of self._output_type by invoking its constructor.
        '''
        # checking data type
        self.check_type(data)
        return self._output_type(self._func(data.data))


class CheckRangeLMeteo3Filter(FilterData):
    """!
    Checks the range of data of some sensor types in MeteoDataFrame

    Input: MeteoDataFrame
    Output: MeteoDataFrame

    Checks, if t_air, t_road, t_underroad, dampness, pressure are in range, defined by constants
    in data_processing.constants

    """
    def __init__(self):
        def check_range_function(df):
            def check_param(df, param, range_min, range_max):
                return df[((df['type'] != param) | ((df['type'] == param) & (df['data'] >= range_min) & (df['data'] <= range_max)))]

            df_new = check_param(df, 't_air', T_MIN, T_MAX)
            df_new = check_param(df_new, 't_road', T_MIN, T_MAX)
            df_new = check_param(df_new, 't_underroad', T_MIN, T_MAX)
            df_new = check_param(df_new, 'dampness', HU_MIN, HU_MAX)
            df_new = check_param(df_new, 'pressure', PA_MIN, PA_MAX)
            return df_new

        super().__init__(input_type=MeteoDataFrame, output_type=MeteoDataFrame, func=check_range_function)
        pass


class GroupLMeteo3ByStationDataFilter(FilterData):
    """!
    Groupes data by one station polling, accumulating station_id, date_time, station_data_id,
    data: t_air, t_road, t_underroad, dampness, pressure

    Input: MeteoDataFrame
    Output: MeteoGroupedByStationData


    """
    def __init__(self):
        def group_by_station_data(df):
            columns = ['t_air', 't_road', 't_underroad',  'dampness', 'pressure']
            dfs_temp = []

            for column in columns:
                df_temp = df[df['type'] == column]
                df_temp = df_temp[['station_id', 'date_time', 'station_data_id', 'data']]
                df_temp = df_temp.rename(columns={'data': column})
                dfs_temp.append(df_temp)

            df_res = pandas.merge(dfs_temp[0], dfs_temp[1], on=['station_id', 'date_time', 'station_data_id'], how='outer')
            for i in range(2, len(columns)):
                df_res = pandas.merge(df_res, dfs_temp[i], on=['station_id', 'date_time', 'station_data_id'],
                                      how='outer')
            return df_res.sort_values(['station_id', 'date_time'])

        super().__init__(input_type=MeteoDataFrame, output_type=MeteoGroupedByStationData, func=group_by_station_data)
        pass


class SplitGroupedDataFilter(FilterData):
    '''!
    Split data by "correct" time intervals, so that minimum time between two sequent station polling is
    less, than dt_max; and time of continious work of the station is greater, than dT_min

    Input: MeteoGroupedByStationData
    Output: MeteoSplittedArray
    '''
    def split_grouped(df):
        def split_column_greater_difference(df, column, diff_val):
            df_splitted = []
            indexes = df[(df[column].diff() > diff_val)].index.tolist()
            indexes.insert(0, 0)
            indexes.append(df.shape[0])
            for index_i in range(len(indexes) - 1):
                index_1 = indexes[index_i]
                index_2 = indexes[index_i + 1]
                df_temp = df[index_1:index_2]
                if not df_temp.empty:
                    df_splitted.append(df_temp)
            return df_splitted

        # split by station_id
        df_stations = split_column_greater_difference(df, 'station_id', 0)

        dfs_stations = []
        # split by date_time in splitted by station_id
        for df_station in df_stations:
            dfs_stations.extend(split_column_greater_difference(df_station, 'date_time', dt_max))

        dfs_result = []

        # Checking continious work range dT_min
        for df_station in dfs_stations:
            time1 = df_station[:1].reset_index()['date_time']
            time2 = df_station[-1:].reset_index()['date_time']
            if ((time2 - time1 > dT_min).bool()):
                dfs_result.append(df_station)
        return dfs_result

    def __init__(self):
        super().__init__(input_type=MeteoGroupedByStationData, output_type=MeteoSplittedArray, func=SplitGroupedDataFilter.split_grouped)
        pass


class SplittedArrayToSamplesFilter(Filter):
    '''!
    Make samples from array of splitted data. Each interval is represented by MeteoSample.
    Interpolates linearly data inside the interval and produces interpolated values in 0 and 30 minutes
    of every hour.

    Input: MeteoSplittedArray
    Output: MeteoSampleArray

    '''
    def make_sample_struct_from_meteo_splitted_array(meteo_splitted):
        sample_array = []
        for correct_interval in meteo_splitted.data:
            date_time_values = np.array(correct_interval['date_time'].values, dtype='datetime64')
            date_time_values = (date_time_values - np.datetime64('1970-01-01T00:00:00Z')) / np.timedelta64(1, 's')

            station_id = list(correct_interval['station_id'])[0]
            # print(station_id)

            first_time = timestamp_to_datetime64(date_time_values[:1][0])
            first_time_rounded = round_time_to_30min(first_time, 'up')
            last_time = timestamp_to_datetime64(date_time_values[-1:][0])
            last_time_rounded = round_time_to_30min(last_time, 'down')

            # print(first_time)
            # print(first_time_rounded)
            # print(last_time)
            # print(last_time_rounded)
            int_count = (int((last_time_rounded - first_time_rounded).astype('timedelta64[m]').astype(int) / 30))

            sample_dict={}

            for y_column in ['t_air', 't_road', 't_underroad', 'dampness', 'pressure']:
                y_values = correct_interval[y_column]


                interpolation = interp1d(date_time_values, y_values)



                window_dict = {str(val): [] for val in range(-12, 5)}
                window_dict['time_now'] = []

                #date_time features
                window_dict['day'] = []
                window_dict['month'] = []
                window_dict['year'] = []
                window_dict['hour'] = []
                window_dict['day_from_year_start']=[]

                # +12, -7 - skip intervals, where rolling window gets None data
                for i in range(12, int_count-7):
                    for j in range(-12, 5):
                        if j <= 0:
                            time = first_time_rounded + np.timedelta64(int((i + j) * 30), 'm')
                        else:
                            time = first_time_rounded + np.timedelta64(int(i * 30 + j * 60), 'm')
                        if (time >= first_time_rounded) and (time <= last_time_rounded):
                            window_dict[str(j)].append(interpolation(datetime64_to_timestamp(time)))
                        else:
                            window_dict[str(j)].append(None)
                    time_now = first_time_rounded + np.timedelta64(i * 30, 'm')
                    window_dict['time_now'].append(time_now)

                    window_dict['day'].append(time_now.astype(object).day)
                    window_dict['month'].append(time_now.astype(object).month)
                    window_dict['year'].append(time_now.astype(object).year)
                    window_dict['hour'].append(time_now.astype(object).hour)
                    window_dict['day_from_year_start'].append(datetime64_to_days_from_year_start(time_now))


                # print (meteo_splitted.array[0])
                # print (window_dict)
                data_frame = pandas.DataFrame.from_dict(window_dict).rename(
                    columns={str(val): str(0.5 * val) for val in range(-12, 0)})

                sample_dict[y_column]=data_frame

            sample = MeteoSample(station_id, first_time, last_time, sample_dict)
            sample_array.append(sample)
        return sample_array

    def __init__(self):
        super().__init__(input_type=MeteoSplittedArray, output_type=MeteoSampleArray, func=SplittedArrayToSamplesFilter.make_sample_struct_from_meteo_splitted_array)
        pass