import numpy as np
import datetime
import calendar

def frange(x=0, y=None, jump=1):
    if y is None:
        y = x
        x = 0
    while x < y:
        yield x
    x += jump

def datetime64_to_timestamp(dt):
    return (dt - np.datetime64('1970-01-01T00:00:00Z')) / np.timedelta64(1, 's')

def timestamp_to_datetime64(timestamp):
    return timestamp * np.timedelta64(1, 's') + np.datetime64('1970-01-01T00:00:00Z')

def round_time_to_30min(dt, where):
    tm = dt.astype(datetime.datetime)
    if where == 'down':
        tm = tm - datetime.timedelta(minutes=tm.minute % 30,
                                     seconds=tm.second)
    elif where == 'up':
        flag_60=False
        if tm.minute>30:
            flag_60 = True
        elif (tm.minute==30) and(tm.second > 0):
            flag_60 = True
        tm = tm + datetime.timedelta(minutes=60 if flag_60 else 30)\
             - datetime.timedelta(minutes=tm.minute % 30,
                                     seconds=tm.second)
    else:
        raise ValueError("where should be 'up' or 'down'")

    return np.datetime64(tm)

def datetime64_to_days_from_year_start(dt):
    year = dt.astype(object).year
    month = dt.astype(object).month
    day = dt.astype(object).day

    days = 0
    for i in range(month-1):
        days += calendar.monthrange(year, i+1)[1]
    days += day
    return days
