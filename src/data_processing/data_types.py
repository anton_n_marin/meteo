'''!
Defines hierarchy of types of data, used in the project
'''
from abc import ABCMeta


class DataAbstract:
    '''!
    @brief Defines abstract type of data, which is used for any data in this project

    This class is used as input/output type for all data operations and classes.
    '''
    __metaclass__ = ABCMeta


class MeteoDataFrame(DataAbstract):
    """! @brief A common meteo data class, based on pandas.DataFrame
    It is a wrapper of pandas.Dataframe class. Exact types of data, obtained by specific transformation are to
    be inherited from this class. Any subtype should contain 'data' field
    """

    def __init__(self, dataframe=None):
        '''!
        A constructor, which defines member data_frame, which can be constructed from existing pandas dataframe
        @param dataframe pandas DataFrame stored in self.data, optional
        '''
        self.data = dataframe

    def __str__(self):
        '''!
        Calls __str__ method of self.data
        '''
        return self.data.__str__()

    def to_csv(self, path_or_buf=None):
        '''!
        @brief Defines output to csv by executing respective method on self.data_frame
        '''
        self.data.to_csv(path_or_buf=path_or_buf)


class MeteoDataArray(DataAbstract):
    """! @brief A common meteo data class, based on python lists
       It is a wrapper of Python list. Exact types of data, obtained by specific transformation are to
       be inherited from this class. Any subtype should contain 'data' field
       """

    def __init__(self, array=None):
        '''!
        A constructor, which defines member array, which can be constructed from existing list
        @param array : python array, optional
        '''
        self.data = array

    def __str__(self):
        return self.data.__str__()


class MeteoGroupedByStationData(MeteoDataFrame):
    '''!
    Defines data, grouped by one station polling, accumulating station_id, date_time, station_data_id,
    data: t_air, t_road, t_underroad, dampness, pressure
    '''
    def __init__(self, dataframe=None):
        super().__init__(dataframe)
        pass


class MeteoSplittedArray(MeteoDataArray):
    '''!
    Split data by "correct" time intervals, so that minimum time between two sequent station polling is
    less, than dt_max; and time of continious work of the station is greater, than dT_min
    '''
    def __init__(self, array=None):
        super().__init__(array=array)
        pass

class MeteoSample(DataAbstract):
    '''!
    Defines a structure, representing sample of the data ('pattern' in terms of previous version)
    '''
    def __init__(self, station_id, first_time, last_time, sample_dict):
        '''!
        Constructor defines class fields.
        @param station_id - id of the station
        @param first_time - datetime from db, first station polling time in the interval
        @param last_time - datetime from db, last station polling time in the interval
        @param sample_dict - dictionary with keys: (t_air, t_road, t_underroad, dampness, pressure) and
        values - interpolated data in 0 and 30 minutes inside the interval

        '''
        self.station_id = station_id
        self.first_time = first_time
        self.last_time = last_time
        self.sample_dict = sample_dict

    def __str__(self):
        '''!
        Makes printable string
        '''
        s = ''
        s += '==========\n'
        s += 'station_id=' + str(self.station_id) + '\n'
        s += 'first_time=' + str(self.first_time) + '\n'
        s += 'last_time=' + str(self.last_time) + '\n'
        for key, value in self.sample_dict.items():
            s += str(key)+':' + '\n'
            s += str(value) + '\n'
            s += '-----' + '\n'
        return s


class MeteoSampleArray(MeteoDataArray):
    '''!
    Defines array of MeteoSample
    '''
    def __init__(self, array):
        super().__init__(array=array)
        pass

    def __str__(self):
        '''!
        Makes printable string
        '''
        s = ''
        for elem in self.data:
            s += str(elem)
        return s