import pandas

from data_processing.data_types import MeteoDataFrame
from abc import ABCMeta

class AbstractDataExtractor:
    __metaclass__ = ABCMeta
    def extract(self):
        pass

class LMeteo3DBExtractor(AbstractDataExtractor):
    """!
    @brief Extract data from lmeteo3 database
    """
    def __init__(self):
        pass

    @staticmethod
    def extract(db_connection, stations=None, datetime_start=None, datetime_end=None, sensor_types=None, limit=None):
        """!
        @brief Creates MeteoDataFrame data from lmeteo3 database

        @param db_connection SQLAlchemy connectable
        @param stations Array of stations to grab data from, defined by station_def.id
        @param datetime_start Datetime, from which to grab the data, defined by station_data.date_time
        @param datetime_end Datetime, till which to grab the data, defined by station_data.date_time
        @param sensor_types Array of sensor types, defined by sensor_type.id
        @param limit Limits a number of rows in resulting dataframe
        """
        # create query
        sql_query='select station_data_id, sensor_id, data, sensor_def.station_id, sensor_type_id, active_state, ' + \
                  'name, rule_id, rule_ordr, direction, lane_traffic, date_time, poll_interval, fake_data, ' + \
                  'type, unit_of_measure, description from sensor_data join ' + \
            'sensor_def on sensor_def.id=sensor_data.sensor_id join station_data on ' + \
            'station_data.id=sensor_data.station_data_id join sensor_type ' + \
            'on sensor_type.id=sensor_def.sensor_type_id '

        # check if we have 'where' clause
        empty_where = (stations is None) and (datetime_start is None) and (datetime_end is None) and (sensor_types is None)
        if not empty_where:
            sql_query += 'where '

            if stations is not None:
                sql_query += 'station_data.station_id in ('+str(stations)[1:-1]+') '
            else:
                sql_query += 'true '

            if sensor_types is not None:
                sql_query += 'and sensor_def.sensor_type_id in ('+str(sensor_types)[1:-1]+') '

            if datetime_start is not None:
                sql_query += 'and station_data.date_time >\''+str(datetime_start)+'\' '

            if datetime_end is not None:
                sql_query += 'and station_data.date_time <\''+str(datetime_end)+'\' '

        if limit is not None:
            sql_query += 'limit '+str(limit)
        sql_query += ';'
        print (sql_query)

        df = pandas.read_sql(con=db_connection, sql=sql_query)
        return MeteoDataFrame(df)