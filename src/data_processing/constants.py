"""!
Defines some useful constants
"""
# Sensors range constants
T_MIN = -80*10
T_MAX = 80*10
HU_MIN = 0*10
HU_MAX = 100*10
PA_MIN = 700
PA_MAX = 800

dt_max = '2h'
dT_min = '12h'
